exports.css = {
  loginError: _ => '.global-error',
  rectangleBox: _ => '.ng-element.el-box.text-editor-light',
  emptyProject: _ => '.inside-project.project-empty',
  newPrototype: _ => '.start-option.start-from-scratch',
  preview: _ => '.ds-icon--font__arrow-play',
  addComment: _ => '.icon-general-comment',
  previewCanvas: _ => '.ng-canvas',
  previewActions: _ => '.main-actions',
  commentsBox: _ => '.comments-group',
  newComment: _ => '.fake-textarea.g-textarea',
  singleComment: _ => '.single-comment'
};

exports.id = {
  loginForm: _ => 'login-form',
  loginEmail: _ => 'login-login',
  loginEmailLabel: _ => 'login-login',
  loginPassword: _ => 'login-password',
  loginPasswordLabel: _ => 'login-password',
  loginSubmit: _ => 'loginform_button1',
  projectBar: _ => 'project-bar',
  newProjectModal: _ => 'project-new-modal-box',
  newProjectName: _ => 'project-name',
  toolbar: _ => 'toolbar',
  canvas: _ => 'canvas',
  topbar: _ => 'top-bar'
};

exports.href = {
  newProject: _ => '#new-project',
  toolbarItemBox: _ => '1791'
};