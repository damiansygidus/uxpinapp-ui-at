export const switchToTab = (tab) => {
  browser.getAllWindowHandles().then(function(handles) {
    browser.switchTo().window(handles[tab]);
  });
}
