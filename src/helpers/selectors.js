export const selectByCss = css => element(by.css(css));
export const selectById = id => element(by.css(`[id="${id}"]`));
export const selectForId = id => element(by.css(`[for="${id}"]`));
export const selectByHref = href => element(by.css(`a[href*="${href}"]`));
