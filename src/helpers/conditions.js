import { timers } from '../constants.js';

const EC = protractor.ExpectedConditions;

/**
 * Wait for URL to match a string or regex
 * @param {string|RegExp} expectedUrl
 * @param {number} timer
 * @example
 * expect(url(TIMER)('http://app.uxpin.com/'))
 */
export const url = (timer) => (expectedUrl) => {
  return browser.wait(
    () => browser.getCurrentUrl().then(url =>
      expectedUrl instanceof RegExp
        ? expectedUrl.test(url)
        : expectedUrl === url
    ),
    timer || timers.render,
    `Timed out waiting for URL "${expectedUrl}"`
  );
}

/**
 * Wait for user to be logged in
 */
export function loggedIn() {
  return browser.wait(
    () => browser.getCurrentUrl().then(url =>
      url.includes("dashboard") 
    ),
    timers.init,
    `Timed out waiting to log in`
  );
}

/**
 * Wait for page fragment to appear on the page
 * @param {numer} timer
 * @example
 * expect(visible(TIMER)(HeaderProfile))
 * expect(visible(TIMER)(selectHeaderProfile))
 */
export const visible = (timer) => (fragment) =>
  browser.wait(
    EC.visibilityOf(fragment),
    timer || timers.render,
    `Timed out waiting for visibility of ${fragment}`
  )

/**
 * Wait for page fragment to disappear from the page
 * @param {number} timer
 * @example
 * expect(invisible(TIMER)(HeaderProfile))
 * expect(invisible(TIMER)(selectHeaderProfile))
 */
export const invisible = (timer) => (fragment) =>
  browser.wait(
      EC.invisibilityOf(fragment),
      timer || timers.render,
      `Timed out waiting for invisibility of ${fragment}`
  );

export const visibleAfterRender = visible(timers.render);
export const visibleAfterRequest = visible(timers.request);
export const visibleAfterTransition = visible(timers.transition);
export const visibleAfterInit = visible(timers.init);

export const invisibleAfterRender = invisible(timers.render);
export const invisibleAfterRequest = invisible(timers.request);
export const invisibleAfterTransition = invisible(timers.transition);
export const invisibleAfterInit = invisible(timers.init);
