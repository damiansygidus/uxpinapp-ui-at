import { compose } from 'redux';
import { selectByCss } from '../../helpers/selectors.js';
import { css } from '../../protractor-keys.js';
import CommentsBox from './comments-box.js';
import { visibleAfterRender } from '../../helpers/conditions.js';
import { enter } from '../../helpers/keyboard.js'

export const previewActions = compose(selectByCss, css.previewActions);
export const addComment = compose(selectByCss, css.addComment);
export const previewCanvas = compose(selectByCss, css.previewCanvas);

export default class PreviewActions {
  constructor() {
    this.$ = previewActions();
    this.addComment = _ => addComment();
    this.previewCanvas = _ => previewCanvas();
  }

  addNewComment(comment) {
    this.addComment().click();
    this.previewCanvas().click();
    const commentsBox = new CommentsBox();
    
    expect(visibleAfterRender(commentsBox.$));
    
    commentsBox.newCommentInput().clear().sendKeys(comment);
    enter();
  }
}
