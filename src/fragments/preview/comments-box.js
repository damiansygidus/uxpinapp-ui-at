import { compose } from 'redux';
import { selectByCss } from '../../helpers/selectors.js';
import { css } from '../../protractor-keys.js';

export const commentsBox = compose(selectByCss, css.commentsBox);
export const newComment = compose(selectByCss, css.newComment);
export const singleComment = compose(selectByCss, css.singleComment);

export default class CommentsBox {
  constructor() {
    this.$ = commentsBox();
    this.newCommentInput = _ => newComment();
    this.singleComment = _ => singleComment();
  }
}
