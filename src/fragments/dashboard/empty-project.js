import { compose } from 'redux';
import { selectByCss } from '../../helpers/selectors.js';
import { css } from '../../protractor-keys.js';
import { timers } from '../../constants.js';

export const emptyProject = compose(selectByCss, css.emptyProject);
export const newPrototype = compose(selectByCss, css.newPrototype);

export default class EmptyProject {
  constructor() {
    this.$ = emptyProject();
    this.newPrototypeButton = _ => newPrototype();
  }

  createNewPrototype() {
    browser.driver.sleep(timers.animation)
    this.newPrototypeButton().click();
  }
}