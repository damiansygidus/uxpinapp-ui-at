import { compose } from 'redux';
import { selectById, selectByCss } from '../../helpers/selectors.js';
import { id, css } from '../../protractor-keys.js';

export const topbar = compose(selectById, id.topbar);
export const preview = compose(selectByCss, css.preview);

export default class Topbar {
  constructor() {
    this.$ = topbar();
    this.previewProject = _ => preview();
  }

  openPreview() {
    this.previewProject().click();
  }
}