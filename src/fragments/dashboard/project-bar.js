import { compose } from 'redux';
import { selectById, selectByHref } from '../../helpers/selectors.js';
import { id, href } from '../../protractor-keys.js';
import { enter } from '../../helpers/keyboard.js'
import NewProjectModal from './new-project-modal.js'
import { visibleAfterRender } from '../../helpers/conditions.js'

export const projectBar = compose(selectById, id.projectBar);
export const newProject = compose(selectByHref, href.newProject);

export default class ProjectBar {
  constructor() {
    this.$ = projectBar();
    this.newProjectButton = _ => newProject();
  }

  createNewProject(name) {
    this.newProjectButton().click();
    const newProjectModal = new NewProjectModal();
    
    expect(visibleAfterRender(newProjectModal.$));
    
    newProjectModal.newProjectNameInput().clear().sendKeys(name);
    enter();
  }
}