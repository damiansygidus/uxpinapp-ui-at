import { compose } from 'redux';
import { selectById } from '../../helpers/selectors.js';
import { id } from '../../protractor-keys.js';

const newProjectModal = compose(selectById, id.newProjectModal);
const newProjectName = compose(selectById, id.newProjectName);

export default class NewProjectModal {
  constructor() {
    this.$ = newProjectModal();
    this.newProjectNameInput = _ => newProjectName();
  }
}
