import { compose } from 'redux';
import { selectByCss, selectById } from '../../helpers/selectors.js';
import { css, id } from '../../protractor-keys.js';

export const canvas = compose(selectById, id.canvas);
export const rectangleBox = compose(selectByCss, css.rectangleBox);

export default class Canvas {
  constructor() {
    this.$ = canvas();
    this.rectangleBoxOnCanvas = _ => rectangleBox();
  }
}