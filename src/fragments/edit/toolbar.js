import { compose } from 'redux';
import { selectByHref, selectById } from '../../helpers/selectors.js';
import { href, id } from '../../protractor-keys.js';

export const toolbar = compose(selectById, id.toolbar);
export const toolbarItemBox = compose(selectByHref, href.toolbarItemBox);

export default class Toolbar {
  constructor() {
    this.$ = toolbar();
    this.toolbarItemBox = _ => toolbarItemBox();
  }

  createBoxOnCanvas() {
    this.toolbarItemBox().click();
  }
}