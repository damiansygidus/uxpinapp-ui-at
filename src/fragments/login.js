import { compose } from 'redux';
import { selectById, selectByCss, selectForId } from '../helpers/selectors.js';
import { id, css } from '../protractor-keys';

export const loginForm = compose(selectById, id.loginForm);
export const loginEmail= compose(selectById, id.loginEmail);
export const loginEmailLabel= compose(selectForId, id.loginEmailLabel);
export const loginPassword = compose(selectById, id.loginPassword);
export const loginPasswordLabel = compose(selectForId, id.loginPasswordLabel);
export const loginSubmit = compose(selectById, id.loginSubmit);
export const loginError = compose(selectByCss, css.loginError);

export default class LoginForm {
  constructor() {
    this.$ = loginForm();
    this.emailInput = _ => loginEmail();
    this.emailLabel = _ => loginEmailLabel();
    this.passwordInput = _ => loginPassword();
    this.passwordLabel = _ => loginPasswordLabel();
    this.submitButton = _ => loginSubmit();
    this.error = _ => loginError();
  }

  loginAs(credentials) {
    this.emailInput().clear().sendKeys(credentials.email);
    this.passwordInput().clear().sendKeys(credentials.password);
    this.submitButton().click();
  }
}
