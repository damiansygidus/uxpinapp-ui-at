const credentials = require('../../../fixtures/credentials.js');
import { LoginRoute } from '../../../routes';
import LoginForm from '../../../fragments/login.js';
import { url, loggedIn, visibleAfterTransition, visibleAfterInit, invisibleAfterInit, visibleAfterRender, visibleAfterRequest } from '../../../helpers/conditions.js';
import { timers } from '../../../constants.js';
import ProjectBar from '../../../fragments/dashboard/project-bar.js';
import EmptyProject from '../../../fragments/dashboard/empty-project.js';
import Toolbar from '../../../fragments/edit/toolbar.js';
import Canvas from '../../../fragments/edit/canvas.js';
import Topbar from '../../../fragments/dashboard/topbar.js';
import { switchToTab } from '../../../helpers/actions.js';
import PreviewActions from '../../../fragments/preview/preview-actions.js';
import CommentsBox from '../../../fragments/preview/comments-box';

describe('Simple project creation flow', function () {
  beforeEach(() => {
    browser.waitForAngularEnabled(false);
  });
  
  it('logs in to dashboard', () => {
    (new LoginRoute()).get();
    const loginForm = new LoginForm();
    
    expect(visibleAfterTransition(loginForm.$));
    expect(loginForm.emailLabel().getText()).toBe('Email:');
    expect(loginForm.emailInput().getText()).toBe('');
    expect(loginForm.passwordLabel().getText()).toBe('Password:');
    expect(loginForm.passwordInput().getText()).toBe('');
    expect(loginForm.submitButton().getText()).toBe('Log in');
    
    loginForm.loginAs(credentials['trial-user']);
    
    loggedIn();
  });
  
  it('creates a new project', () => {
    const projectBar = new ProjectBar();
    
    expect(visibleAfterTransition(projectBar.$));

    projectBar.createNewProject("simpleCreationFlow");
  });
  
  it('creates a new prototype', () => {
    const emptyProject = new EmptyProject();
    
    expect(visibleAfterInit(emptyProject.$));
    
    emptyProject.createNewPrototype();

    expect(invisibleAfterInit(emptyProject.$));
    expect(url(timers.transition)(new RegExp(browser.params.app + "\/edit\/.*", "i")))
  });
  
  it('creates a box on canvas', () => {
    const toolbar = new Toolbar();
    
    expect(visibleAfterInit(toolbar.$));
    
    toolbar.createBoxOnCanvas(); 
    
    const canvas = new Canvas();
    
    expect(visibleAfterRender(canvas.$));
    expect(visibleAfterRender(canvas.rectangleBoxOnCanvas()));
  });
  
  it('opens project preview', () => {
    const topbar = new Topbar();
    
    expect(visibleAfterRender(topbar.$));
    
    topbar.openPreview();
    
    switchToTab(1);
    
    expect(url(timers.transition)(new RegExp(browser.params.preview + ".*\/pages\/.*", "i")))
  });
  
  it('adds new comment on preview', () => {
    const previewActions = new PreviewActions();
    
    expect(visibleAfterInit(previewActions.$));
    expect(visibleAfterInit(previewActions.previewCanvas()));
    
    previewActions.addNewComment("Some funny comment with joke");

    const commentBox = new CommentsBox();
    
    expect(visibleAfterRequest(commentBox.singleComment()));
  });
});
