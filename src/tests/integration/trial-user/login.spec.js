const credentials = require('../../../fixtures/credentials.js');
import { LoginRoute } from '../../../routes';
import LoginForm from '../../../fragments/login.js';
import { loggedIn, visibleAfterRequest } from '../../../helpers/conditions.js';

describe('Login page', function () {
  beforeEach(() => {
    browser.waitForAngularEnabled(false);
    (new LoginRoute()).get();

    expect(visibleAfterRequest((new LoginForm()).$));
  });
  
  it('shows login form with username, password and submit button', () => {
    const loginForm = new LoginForm();
    
    expect(loginForm.emailLabel().getText()).toBe('Email:');
    expect(loginForm.emailInput().getText()).toBe('');
    expect(loginForm.passwordLabel().getText()).toBe('Password:');
    expect(loginForm.passwordInput().getText()).toBe('');
    expect(loginForm.submitButton().getText()).toBe('Log in');
  });
  
  it('logs in to dashboard', () => {
    (new LoginForm()).loginAs(credentials['trial-user']);
    
    loggedIn();
  });
});
