'use strict';

var SpecReporter = require('jasmine-spec-reporter').SpecReporter;

var _require = require('./constants'),
    sizes = _require.sizes;

exports.config = {
  params: {
    app: 'https://app.uxpin.com',
    preview: 'https://preview.uxpin.com'
  },

  framework: 'jasmine2',

  directConnect: true,

  specs: ['./**/*.spec.js'],

  suites: {
    'trial-user': ['./tests/*/trial-user/*.spec.js']
  },

  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 1,
    chromeOptions: {
      args: ['--no-sandbox', '--disable-infobars', '--disable-extensions', '--window-size=1920,1080'],
      prefs: {
        'profile.password_manager_enabled': false,
        'credentials_enable_service': false,
        'password_manager_enabled': false
      }
    }
  }
};