'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginError = exports.loginSubmit = exports.loginPasswordLabel = exports.loginPassword = exports.loginEmailLabel = exports.loginEmail = exports.loginForm = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../helpers/selectors.js');

var _protractorKeys = require('../protractor-keys');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var loginForm = exports.loginForm = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.loginForm);
var loginEmail = exports.loginEmail = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.loginEmail);
var loginEmailLabel = exports.loginEmailLabel = (0, _redux.compose)(_selectors.selectForId, _protractorKeys.id.loginEmailLabel);
var loginPassword = exports.loginPassword = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.loginPassword);
var loginPasswordLabel = exports.loginPasswordLabel = (0, _redux.compose)(_selectors.selectForId, _protractorKeys.id.loginPasswordLabel);
var loginSubmit = exports.loginSubmit = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.loginSubmit);
var loginError = exports.loginError = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.loginError);

var LoginForm = function () {
  function LoginForm() {
    _classCallCheck(this, LoginForm);

    this.$ = loginForm();
    this.emailInput = function (_) {
      return loginEmail();
    };
    this.emailLabel = function (_) {
      return loginEmailLabel();
    };
    this.passwordInput = function (_) {
      return loginPassword();
    };
    this.passwordLabel = function (_) {
      return loginPasswordLabel();
    };
    this.submitButton = function (_) {
      return loginSubmit();
    };
    this.error = function (_) {
      return loginError();
    };
  }

  _createClass(LoginForm, [{
    key: 'loginAs',
    value: function loginAs(credentials) {
      this.emailInput().clear().sendKeys(credentials.email);
      this.passwordInput().clear().sendKeys(credentials.password);
      this.submitButton().click();
    }
  }]);

  return LoginForm;
}();

exports.default = LoginForm;