'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.previewCanvas = exports.addComment = exports.previewActions = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

var _commentsBox = require('./comments-box.js');

var _commentsBox2 = _interopRequireDefault(_commentsBox);

var _conditions = require('../../helpers/conditions.js');

var _keyboard = require('../../helpers/keyboard.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var previewActions = exports.previewActions = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.previewActions);
var addComment = exports.addComment = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.addComment);
var previewCanvas = exports.previewCanvas = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.previewCanvas);

var PreviewActions = function () {
  function PreviewActions() {
    _classCallCheck(this, PreviewActions);

    this.$ = previewActions();
    this.addComment = function (_) {
      return addComment();
    };
    this.previewCanvas = function (_) {
      return previewCanvas();
    };
  }

  _createClass(PreviewActions, [{
    key: 'addNewComment',
    value: function addNewComment(comment) {
      this.addComment().click();
      this.previewCanvas().click();
      var commentsBox = new _commentsBox2.default();

      expect((0, _conditions.visibleAfterRender)(commentsBox.$));

      commentsBox.newCommentInput().clear().sendKeys(comment);
      (0, _keyboard.enter)();
    }
  }]);

  return PreviewActions;
}();

exports.default = PreviewActions;