'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.singleComment = exports.newComment = exports.commentsBox = undefined;

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var commentsBox = exports.commentsBox = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.commentsBox);
var newComment = exports.newComment = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.newComment);
var singleComment = exports.singleComment = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.singleComment);

var CommentsBox = function CommentsBox() {
  _classCallCheck(this, CommentsBox);

  this.$ = commentsBox();
  this.newCommentInput = function (_) {
    return newComment();
  };
  this.singleComment = function (_) {
    return singleComment();
  };
};

exports.default = CommentsBox;