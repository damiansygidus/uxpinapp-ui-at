'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rectangleBox = exports.canvas = undefined;

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var canvas = exports.canvas = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.canvas);
var rectangleBox = exports.rectangleBox = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.rectangleBox);

var Canvas = function Canvas() {
  _classCallCheck(this, Canvas);

  this.$ = canvas();
  this.rectangleBoxOnCanvas = function (_) {
    return rectangleBox();
  };
};

exports.default = Canvas;