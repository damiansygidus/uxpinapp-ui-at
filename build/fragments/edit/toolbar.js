'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toolbarItemBox = exports.toolbar = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var toolbar = exports.toolbar = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.toolbar);
var toolbarItemBox = exports.toolbarItemBox = (0, _redux.compose)(_selectors.selectByHref, _protractorKeys.href.toolbarItemBox);

var Toolbar = function () {
  function Toolbar() {
    _classCallCheck(this, Toolbar);

    this.$ = toolbar();
    this.toolbarItemBox = function (_) {
      return toolbarItemBox();
    };
  }

  _createClass(Toolbar, [{
    key: 'createBoxOnCanvas',
    value: function createBoxOnCanvas() {
      this.toolbarItemBox().click();
    }
  }]);

  return Toolbar;
}();

exports.default = Toolbar;