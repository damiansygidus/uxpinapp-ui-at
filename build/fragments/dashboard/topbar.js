'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.preview = exports.topbar = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var topbar = exports.topbar = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.topbar);
var preview = exports.preview = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.preview);

var Topbar = function () {
  function Topbar() {
    _classCallCheck(this, Topbar);

    this.$ = topbar();
    this.previewProject = function (_) {
      return preview();
    };
  }

  _createClass(Topbar, [{
    key: 'openPreview',
    value: function openPreview() {
      this.previewProject().click();
    }
  }]);

  return Topbar;
}();

exports.default = Topbar;