'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var newProjectModal = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.newProjectModal);
var newProjectName = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.newProjectName);

var NewProjectModal = function NewProjectModal() {
  _classCallCheck(this, NewProjectModal);

  this.$ = newProjectModal();
  this.newProjectNameInput = function (_) {
    return newProjectName();
  };
};

exports.default = NewProjectModal;