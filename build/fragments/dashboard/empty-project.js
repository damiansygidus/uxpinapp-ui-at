'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newPrototype = exports.emptyProject = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

var _constants = require('../../constants.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var emptyProject = exports.emptyProject = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.emptyProject);
var newPrototype = exports.newPrototype = (0, _redux.compose)(_selectors.selectByCss, _protractorKeys.css.newPrototype);

var EmptyProject = function () {
  function EmptyProject() {
    _classCallCheck(this, EmptyProject);

    this.$ = emptyProject();
    this.newPrototypeButton = function (_) {
      return newPrototype();
    };
  }

  _createClass(EmptyProject, [{
    key: 'createNewPrototype',
    value: function createNewPrototype() {
      browser.driver.sleep(_constants.timers.animation);
      this.newPrototypeButton().click();
    }
  }]);

  return EmptyProject;
}();

exports.default = EmptyProject;