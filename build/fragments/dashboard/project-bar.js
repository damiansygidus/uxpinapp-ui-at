'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newProject = exports.projectBar = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _redux = require('redux');

var _selectors = require('../../helpers/selectors.js');

var _protractorKeys = require('../../protractor-keys.js');

var _keyboard = require('../../helpers/keyboard.js');

var _newProjectModal = require('./new-project-modal.js');

var _newProjectModal2 = _interopRequireDefault(_newProjectModal);

var _conditions = require('../../helpers/conditions.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var projectBar = exports.projectBar = (0, _redux.compose)(_selectors.selectById, _protractorKeys.id.projectBar);
var newProject = exports.newProject = (0, _redux.compose)(_selectors.selectByHref, _protractorKeys.href.newProject);

var ProjectBar = function () {
  function ProjectBar() {
    _classCallCheck(this, ProjectBar);

    this.$ = projectBar();
    this.newProjectButton = function (_) {
      return newProject();
    };
  }

  _createClass(ProjectBar, [{
    key: 'createNewProject',
    value: function createNewProject(name) {
      this.newProjectButton().click();
      var newProjectModal = new _newProjectModal2.default();

      expect((0, _conditions.visibleAfterRender)(newProjectModal.$));

      newProjectModal.newProjectNameInput().clear().sendKeys(name);
      (0, _keyboard.enter)();
    }
  }]);

  return ProjectBar;
}();

exports.default = ProjectBar;