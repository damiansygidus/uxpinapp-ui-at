'use strict';

var _routes = require('../../../routes');

var _login = require('../../../fragments/login.js');

var _login2 = _interopRequireDefault(_login);

var _conditions = require('../../../helpers/conditions.js');

var _constants = require('../../../constants.js');

var _projectBar = require('../../../fragments/dashboard/project-bar.js');

var _projectBar2 = _interopRequireDefault(_projectBar);

var _emptyProject = require('../../../fragments/dashboard/empty-project.js');

var _emptyProject2 = _interopRequireDefault(_emptyProject);

var _toolbar = require('../../../fragments/edit/toolbar.js');

var _toolbar2 = _interopRequireDefault(_toolbar);

var _canvas = require('../../../fragments/edit/canvas.js');

var _canvas2 = _interopRequireDefault(_canvas);

var _topbar = require('../../../fragments/dashboard/topbar.js');

var _topbar2 = _interopRequireDefault(_topbar);

var _actions = require('../../../helpers/actions.js');

var _previewActions = require('../../../fragments/preview/preview-actions.js');

var _previewActions2 = _interopRequireDefault(_previewActions);

var _commentsBox = require('../../../fragments/preview/comments-box');

var _commentsBox2 = _interopRequireDefault(_commentsBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var credentials = require('../../../fixtures/credentials.js');


describe('Simple project creation flow', function () {
  beforeEach(function () {
    browser.waitForAngularEnabled(false);
  });

  it('logs in to dashboard', function () {
    new _routes.LoginRoute().get();
    var loginForm = new _login2.default();

    expect((0, _conditions.visibleAfterTransition)(loginForm.$));
    expect(loginForm.emailLabel().getText()).toBe('Email:');
    expect(loginForm.emailInput().getText()).toBe('');
    expect(loginForm.passwordLabel().getText()).toBe('Password:');
    expect(loginForm.passwordInput().getText()).toBe('');
    expect(loginForm.submitButton().getText()).toBe('Log in');

    loginForm.loginAs(credentials['trial-user']);

    (0, _conditions.loggedIn)();
  });

  it('creates a new project', function () {
    var projectBar = new _projectBar2.default();

    expect((0, _conditions.visibleAfterTransition)(projectBar.$));

    projectBar.createNewProject("simpleCreationFlow");
  });

  it('creates a new prototype', function () {
    var emptyProject = new _emptyProject2.default();

    expect((0, _conditions.visibleAfterInit)(emptyProject.$));

    emptyProject.createNewPrototype();

    expect((0, _conditions.invisibleAfterInit)(emptyProject.$));
    expect((0, _conditions.url)(_constants.timers.transition)(new RegExp(browser.params.app + "\/edit\/.*", "i")));
  });

  it('creates a box on canvas', function () {
    var toolbar = new _toolbar2.default();

    expect((0, _conditions.visibleAfterInit)(toolbar.$));

    toolbar.createBoxOnCanvas();

    var canvas = new _canvas2.default();

    expect((0, _conditions.visibleAfterRender)(canvas.$));
    expect((0, _conditions.visibleAfterRender)(canvas.rectangleBoxOnCanvas()));
  });

  it('opens project preview', function () {
    var topbar = new _topbar2.default();

    expect((0, _conditions.visibleAfterRender)(topbar.$));

    topbar.openPreview();

    (0, _actions.switchToTab)(1);

    expect((0, _conditions.url)(_constants.timers.transition)(new RegExp(browser.params.preview + ".*\/pages\/.*", "i")));
  });

  it('adds new comment on preview', function () {
    var previewActions = new _previewActions2.default();

    expect((0, _conditions.visibleAfterInit)(previewActions.$));
    expect((0, _conditions.visibleAfterInit)(previewActions.previewCanvas()));

    previewActions.addNewComment("Some funny comment with joke");

    var commentBox = new _commentsBox2.default();

    expect((0, _conditions.visibleAfterRequest)(commentBox.singleComment()));
  });
});