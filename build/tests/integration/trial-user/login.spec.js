'use strict';

var _routes = require('../../../routes');

var _login = require('../../../fragments/login.js');

var _login2 = _interopRequireDefault(_login);

var _conditions = require('../../../helpers/conditions.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var credentials = require('../../../fixtures/credentials.js');


describe('Login page', function () {
  beforeEach(function () {
    browser.waitForAngularEnabled(false);
    new _routes.LoginRoute().get();

    expect((0, _conditions.visibleAfterRequest)(new _login2.default().$));
  });

  it('shows login form with username, password and submit button', function () {
    var loginForm = new _login2.default();

    expect(loginForm.emailLabel().getText()).toBe('Email:');
    expect(loginForm.emailInput().getText()).toBe('');
    expect(loginForm.passwordLabel().getText()).toBe('Password:');
    expect(loginForm.passwordInput().getText()).toBe('');
    expect(loginForm.submitButton().getText()).toBe('Log in');
  });

  it('logs in to dashboard', function () {
    new _login2.default().loginAs(credentials['trial-user']);

    (0, _conditions.loggedIn)();
  });
});