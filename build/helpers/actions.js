"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var switchToTab = exports.switchToTab = function switchToTab(tab) {
  browser.getAllWindowHandles().then(function (handles) {
    browser.switchTo().window(handles[tab]);
  });
};