"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invisibleAfterInit = exports.invisibleAfterTransition = exports.invisibleAfterRequest = exports.invisibleAfterRender = exports.visibleAfterInit = exports.visibleAfterTransition = exports.visibleAfterRequest = exports.visibleAfterRender = exports.invisible = exports.visible = exports.url = undefined;
exports.loggedIn = loggedIn;

var _constants = require("../constants.js");

var EC = protractor.ExpectedConditions;

/**
 * Wait for URL to match a string or regex
 * @param {string|RegExp} expectedUrl
 * @param {number} timer
 * @example
 * expect(url(TIMER)('http://app.uxpin.com/'))
 */
var url = exports.url = function url(timer) {
  return function (expectedUrl) {
    return browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return expectedUrl instanceof RegExp ? expectedUrl.test(url) : expectedUrl === url;
      });
    }, timer || _constants.timers.render, "Timed out waiting for URL \"" + expectedUrl + "\"");
  };
};

/**
 * Wait for user to be logged in
 */
function loggedIn() {
  return browser.wait(function () {
    return browser.getCurrentUrl().then(function (url) {
      return url.includes("dashboard");
    });
  }, _constants.timers.init, "Timed out waiting to log in");
}

/**
 * Wait for page fragment to appear on the page
 * @param {numer} timer
 * @example
 * expect(visible(TIMER)(HeaderProfile))
 * expect(visible(TIMER)(selectHeaderProfile))
 */
var visible = exports.visible = function visible(timer) {
  return function (fragment) {
    return browser.wait(EC.visibilityOf(fragment), timer || _constants.timers.render, "Timed out waiting for visibility of " + fragment);
  };
};

/**
 * Wait for page fragment to disappear from the page
 * @param {number} timer
 * @example
 * expect(invisible(TIMER)(HeaderProfile))
 * expect(invisible(TIMER)(selectHeaderProfile))
 */
var invisible = exports.invisible = function invisible(timer) {
  return function (fragment) {
    return browser.wait(EC.invisibilityOf(fragment), timer || _constants.timers.render, "Timed out waiting for invisibility of " + fragment);
  };
};

var visibleAfterRender = exports.visibleAfterRender = visible(_constants.timers.render);
var visibleAfterRequest = exports.visibleAfterRequest = visible(_constants.timers.request);
var visibleAfterTransition = exports.visibleAfterTransition = visible(_constants.timers.transition);
var visibleAfterInit = exports.visibleAfterInit = visible(_constants.timers.init);

var invisibleAfterRender = exports.invisibleAfterRender = invisible(_constants.timers.render);
var invisibleAfterRequest = exports.invisibleAfterRequest = invisible(_constants.timers.request);
var invisibleAfterTransition = exports.invisibleAfterTransition = invisible(_constants.timers.transition);
var invisibleAfterInit = exports.invisibleAfterInit = invisible(_constants.timers.init);