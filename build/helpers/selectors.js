"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var selectByCss = exports.selectByCss = function selectByCss(css) {
  return element(by.css(css));
};
var selectById = exports.selectById = function selectById(id) {
  return element(by.css("[id=\"" + id + "\"]"));
};
var selectForId = exports.selectForId = function selectForId(id) {
  return element(by.css("[for=\"" + id + "\"]"));
};
var selectByHref = exports.selectByHref = function selectByHref(href) {
  return element(by.css("a[href*=\"" + href + "\"]"));
};