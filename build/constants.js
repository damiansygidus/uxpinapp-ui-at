"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var second = 1000;

var timers = exports.timers = {
  animation: 1 * second, // force wait for covered elements to be clickable
  render: 2.5 * second, // wait for element to render, including animations etc
  request: 6 * second, // wait for request to API to finish (edit/save/delete etc)
  transition: 10 * second, // wait for route to fully load
  init: 15 * second // wait for initial app load
};