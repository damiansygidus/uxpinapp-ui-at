'use strict';

exports.css = {
  loginError: function loginError(_) {
    return '.global-error';
  },
  rectangleBox: function rectangleBox(_) {
    return '.ng-element.el-box.text-editor-light';
  },
  emptyProject: function emptyProject(_) {
    return '.inside-project.project-empty';
  },
  newPrototype: function newPrototype(_) {
    return '.start-option.start-from-scratch';
  },
  preview: function preview(_) {
    return '.ds-icon--font__arrow-play';
  },
  addComment: function addComment(_) {
    return '.icon-general-comment';
  },
  previewCanvas: function previewCanvas(_) {
    return '.ng-canvas';
  },
  previewActions: function previewActions(_) {
    return '.main-actions';
  },
  commentsBox: function commentsBox(_) {
    return '.comments-group';
  },
  newComment: function newComment(_) {
    return '.fake-textarea.g-textarea';
  },
  singleComment: function singleComment(_) {
    return '.single-comment';
  }
};

exports.id = {
  loginForm: function loginForm(_) {
    return 'login-form';
  },
  loginEmail: function loginEmail(_) {
    return 'login-login';
  },
  loginEmailLabel: function loginEmailLabel(_) {
    return 'login-login';
  },
  loginPassword: function loginPassword(_) {
    return 'login-password';
  },
  loginPasswordLabel: function loginPasswordLabel(_) {
    return 'login-password';
  },
  loginSubmit: function loginSubmit(_) {
    return 'loginform_button1';
  },
  projectBar: function projectBar(_) {
    return 'project-bar';
  },
  newProjectModal: function newProjectModal(_) {
    return 'project-new-modal-box';
  },
  newProjectName: function newProjectName(_) {
    return 'project-name';
  },
  toolbar: function toolbar(_) {
    return 'toolbar';
  },
  canvas: function canvas(_) {
    return 'canvas';
  },
  topbar: function topbar(_) {
    return 'top-bar';
  }
};

exports.href = {
  newProject: function newProject(_) {
    return '#new-project';
  },
  toolbarItemBox: function toolbarItemBox(_) {
    return '1791';
  }
};