# End-to-end testing with Protractor for UXPin App

Check out Protractor at
https://www.protractortest.org/#/

## Setup

### 1. Install Protractor

Use npm to install Protractor globally with:
```
npm i -g protractor
```
This will install two command line tools, `protractor` and `webdriver-manager`. Try running `protractor --version` to make sure it's working.

The `webdriver-manager` is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with:
```
webdriver-manager update
```
Now start up a server with:
```
webdriver-manager start
```
This will start up a Selenium Server and will output a bunch of info logs. Your Protractor test will send requests to this server to control a local browser. You can see information about the status of the server at http://localhost:4444/wd/hub.

### 2. Setup test users

Since most tests require logging into application, you need to provide user credentials. To do this copy `credentials.example.json` into new file `credentials.json` (git should ignore it). Replace placeholders in `credentials.json` with real passwords and you are good to go!

### 3. Run tests!

Run simple integration test with:
```
npm start 
```
or run end-to-end test from test scanario:
```
npm run test:end-to-end:trial-user
```

If you have more questions or if you experience any problems please contact me at *syg.dam(at)gmail.com*
